"use strict";
require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { JWT_SECRET_KEY } = process.env;
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.belongsTo(models.Image, { foreignKey: "avatar", as: "image" });
      User.hasOne(models.Biodata, {
        foreignKey: "user_id",
        as: "biodata",
      });
      User.hasMany(models.History, { foreignKey: "user_id", as: "history" });
    }
    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        name: this.name,
        email: this.email,
      };

      const secretKey = JWT_SECRET_KEY;

      const token = jwt.sign(payload, secretKey);
      return token;
    };

    static authenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({
          where: {
            email: email,
          },
        });
        if (!user)
          return Promise.reject(
            new Error(
              "User not found! Make sure the email and password is correct"
            )
          );

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid)
          return Promise.reject(new Error("Email or password isn't correct!"));

        return Promise.resolve(user);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  User.init(
    {
      f_name: DataTypes.STRING,
      l_name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      avatar: {
        allowNull: true,
        type: DataTypes.INTEGER,
      },
      role: DataTypes.STRING,
      user_type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "User",
    }
  );
  return User;
};
