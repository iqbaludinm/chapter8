const request = require("supertest");
const app = require("../app");
const truncate = require("../helpers/truncate");

// test some endpoints for user
describe("test endpoint createUser", () => {
  test("it should return success for create new user", async () => {
    try {
      await truncate.user();
      const res = await request(app).post("/api/v1/user").send({
        username: "iqbal@mail.com",
        password: "iqbal789",
        biodata_id: 1,
      });

      expect(res.status).toBe(201);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully created user");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
      // console.log(err);
    }
  });

  // negative test
  test("it should return error if same username", async () => {
    try {
      const res = await request(app).post("/api/v1/user").send({
        username: "iqbal@mail.com",
        password: "iqbal789",
        biodata_id: 1,
      });

      expect(res.status).toBe(400);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("error");
      expect(res.body.message).toBe("username is already exist!");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should return all data users", async () => {
    try {
      const res = await request(app).get("/api/v1/user");
      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully retrieve all data user");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("return user with certain id", async () => {
    try {
      const res = await request(app).get("/api/v1/user/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully retrieve data user with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should be updated certain user data", async () => {
    try {
      const res = await request(app).put("/api/v1/user/1").send({
        username: "iqbaludinm",
      });

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully updated data");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("will deleting user data with certain id", async () => {
    try {
      const res = await request(app).delete("/api/v1/user/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully deleted user with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });
});

// test some endpoints for biodata
describe("test endpoint biodata", () => {
  const bio1 = {
    name: "iqbal",
    path_avatar: "D:/Pictures",
    about: "Ini bio Iqbal",
  };

  test("it should return success create for new biodata", async () => {
    try {
      await truncate.bio();
      const res = await request(app).post("/api/v1/biodata").send(bio1);

      expect(res.status).toBe(201);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("Biodata created successfully");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should return all biodata", async () => {
    try {
      const res = await request(app).get("/api/v1/biodata");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("All biodata was retrieved succesfully");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should retrieve certain bio with biodata id", async () => {
    try {
      const res = await request(app).get("/api/v1/biodata/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("success retrived biodata with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should updating biodata", async () => {
    try {
      const res = await request(app).put("/api/v1/biodata/1").send({
        about: "Iqbal addicted to Valo",
      });

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("success updated biodata with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should delete biodata with id", async () => {
    try {
      const res = await request(app).delete("/api/v1/biodata/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("deleted biodata succesfully");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });
});

// test some endpoints for history
describe("test endpoint history", () => {
  const hist = {
    play_time: "1h ago",
    score: 900,
    user_id: 1,
  };

  test("it should return success created for new history", async () => {
    try {
      await truncate.history();
      const res = await request(app).post("/api/v1/history").send(hist);

      expect(res.status).toBe(201);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("new history is present");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should retrieve all histories", async () => {
    try {
      const res = await request(app).get("/api/v1/history");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully retrieved all histories");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should retrieve history with id", async () => {
    try {
      const res = await request(app).get("/api/v1/history/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully retrieved history with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should update certain history with id", async () => {
    try {
      const res = await request(app)
        .put("/api/v1/history/1")
        .send({ play_time: "10s ago" });

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully updated history with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });

  it("should delete certain history", async () => {
    try {
      const res = await request(app)
        .delete("/api/v1/history/1");

      expect(res.status).toBe(200);
      expect(res.body).toHaveProperty("status");
      expect(res.body).toHaveProperty("message");
      expect(res.body).toHaveProperty("data");
      expect(res.body.status).toBe("success");
      expect(res.body.message).toBe("successfully deleted history with id");
    } catch (err) {
      console.log({ error_message: err.message });
      expect(err).toBe("error");
    }
  });
});
