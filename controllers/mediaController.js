const { Image, Video, File } = require("../models");

module.exports = {
  image: async (req, res) => {
    try {
      const imageUrl =
        req.protocol + "://" + req.get("host") + "/image/" + req.file.filename;

      const image = await Image.create({
        title: req.file.filename,
        url: imageUrl,
      });

      res.status(200).json({
        status: true,
        message: "success",
        data: image,
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  video: async (req, res) => {
    try {
      const videoUrl =
        req.protocol + "://" + req.get("host") + "/video/" + req.file.filename;

      const video = await Video.create({
        title: req.file.filename,
        url: videoUrl,
      });

      res.status(200).json({
        status: true,
        message: "success",
        data: video,
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },

  file: async (req, res) => {
    try {
      const fileUrl =
        req.protocol + "://" + req.get("host") + "/file/" + req.file.filename;

      const file = await File.create({
        title: req.file.filename,
        url: fileUrl,
      });

      res.status(200).json({
        status: true,
        message: "success",
        data: file,
      });
    } catch (err) {
      return res.status(500).json({
        status: false,
        message: err.message,
        data: null,
      });
    }
  },
};
