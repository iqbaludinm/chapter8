const { History } = require("../models");

createHistory = async (req, res) => {
  try {
    const { play_time, score, user_id } = req.body;

    if (!user_id) {
      res.status(400).json({
        status: "error",
        message: "user_id is required",
        data: null,
      });
      return;
    }
    
    const newHistory = await History.create({
      play_time,
      score,
      user_id,
    });

    res.status(201).json({
      status: "success",
      message: "new history is present",
      data: newHistory,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

getHistories = async (req, res) => {
  try {
    const histories = await History.findAll({ include: ["player"] });

    res.status(200).json({
      status: "success",
      message: "successfully retrieved all histories",
      data: histories,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

getHistory = async (req, res) => {
  try {
    const history_id = req.params.id;

    const history = await History.findOne({
      where: { id: history_id },
      include: ["player"],
    });

    if (!history) {
      res.status(404).json({
        status: "error",
        message: `history with id ${history_id} doesn't exist`,
        data: null,
      });
      return;
    }

    res.status(200).json({
      status: "success",
      message: `successfully retrieved history with id`,
      data: history,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

updateHistory = async (req, res) => {
  try {
    const history_id = req.params.id;
    const { play_time, score, user_id } = req.body;
    let query = {
      where: { id: history_id },
    };

    const history = await History.findOne({
      where: { id: history_id },
      include: ["player"],
    });

    if (!history) {
      res.status(404).json({
        status: "error",
        message: `history with id ${history_id} doesn't exist`,
        data: null,
      });
      return;
    }

    const updatedHistory = await History.update(
      {
        play_time,
        score,
        user_id,
      },
      query
    );

    res.status(200).json({
      status: "success",
      message: `successfully updated history with id`,
      data: updatedHistory,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

deleteHistory = async (req, res) => {
  try {
    const history_id = req.params.id;

    const history = await History.findOne({
      where: { id: history_id },
      include: ["player"],
    });

    if (!history) {
      res.status(404).json({
        status: "error",
        message: `history with id ${history_id} doesn't exist`,
        data: null,
      });
      return;
    }

    const deletedHistory = await History.destroy({
      where: { id: history_id },
    });

    res.status(200).json({
      status: "success",
      message: `successfully deleted history with id`,
      data: deletedHistory,
    });
  } catch (err) {
    res.status(500).json({
      status: "error",
      message: err.message,
    });
  }
};

module.exports = {
  createHistory,
  getHistories,
  getHistory,
  updateHistory,
  deleteHistory,
};
